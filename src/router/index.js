import VueRouter from 'vue-router';

const routes = [
    { path: '/home', component: {} },
    { path: '/about', component: {} },
];

const router = new VueRouter({
    routes: routes,
    mode: 'history'
});
export default router;
