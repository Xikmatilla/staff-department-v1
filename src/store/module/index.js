
const state = {
    loading: false

};

const getters = {
};

const actions = {
    async getStore({commit}) {
        commit('getAction', false);
    },

};

const mutations = {
    getAction(state, res) {
        state.loading = res;
    },
};

export default  {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};
